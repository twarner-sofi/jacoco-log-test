plugins {
    id("org.barfuin.gradle.jacocolog") version "3.0.0-RC1"
}

repositories {
    mavenCentral()
}
