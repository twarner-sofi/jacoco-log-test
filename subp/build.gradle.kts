plugins {
    kotlin("jvm") version "1.6.21"
    jacoco
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

val jacocoTestReport = tasks.named<JacocoReport>("jacocoTestReport") {
    reports {
        xml.required.set(true)
    }
    dependsOn(tasks.named("test"))
}

tasks.withType<Test>().configureEach {
    finalizedBy(jacocoTestReport) // report is always generated after tests run
}
