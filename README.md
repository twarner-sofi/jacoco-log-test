Running the command

`gradle --console=plain build jacocoAggregatedReport`

Actual output:

```
> Task :subp:processResources NO-SOURCE
> Task :subp:processTestResources NO-SOURCE
> Task :subp:compileKotlin
> Task :subp:compileJava NO-SOURCE
> Task :subp:classes UP-TO-DATE
> Task :subp:inspectClassesForKotlinIC
> Task :subp:jar
> Task :subp:assemble
> Task :jacocoAggregatedReport SKIPPED
> Task :jacocoLogAggregatedCoverage
> Task :subp:compileTestKotlin
> Task :subp:compileTestJava NO-SOURCE
> Task :subp:testClasses UP-TO-DATE
> Task :subp:test
> Task :subp:jacocoTestReport
> Task :subp:check
> Task :subp:build

BUILD SUCCESSFUL in 3s
7 actionable tasks: 7 executed
```

Expected: :jacocoAggregatedReport should not be skipped SKIPPED
